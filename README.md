# indigo-player-test

Test page to play around with [Indigo-Player](https://matvp91.github.io/indigo-player).

## License
Indigo-Player under [Apache License 2.0](https://github.com/matvp91/indigo-player/blob/master/LICENSE), all the rest of this repo is under [Unlicense](https://unlicense.org/).

## Live Demo

[https://projects.gregoryhammond.ca/indigo-player-test/](https://projects.gregoryhammond.ca/indigo-player-test/)